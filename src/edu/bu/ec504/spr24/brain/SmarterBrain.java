package edu.bu.ec504.spr24.brain;

import edu.bu.ec504.spr24.brain.Board.Pos;
import edu.bu.ec504.spr24.sameGameTris.CircleColor;

import javax.swing.*;

/**
 * A smarter brain, for you to produce.
 */
public class SmarterBrain extends Brain {

    private Board currBoard;

    public SmarterBrain() {
        super();
    }

    @Override
    public String myName() {
        return "Smarter Brain";
    }

    @Override
    Pos nextMove() {

        //Initialize a board for me to keep track at locally
        currBoard = new Board();

        //Copies the current board into our temporary board
        for (int xx = 0; xx < myGUI.boardWidth(); xx++) {
            for (int yy = 0; yy < myGUI.boardHeight(); yy++){
                currBoard.modify(xx, yy, myGUI.colorAt(xx, myGUI.boardHeight() - yy - 1));
            }
        }

        //If the tallest item is a certain height (at least 7 for 10 rows)
        if(numberOfItemsInCol(tallest())>myGUI.boardHeight()-4){
            //Perform a safe move getting popping one of the groups in the tallest
            return safeMove(tallest());
        }

        //Returns the greedy move implemented in Lazy Brain
        return greedyMove();
    }

    //Function to find the tallest column
    private int tallest(){
        int max = 0;
        int tall = 0;
        //Iterate through column, sum the number of items, and record the tallest
        for (int i = 0; i<currBoard.columns(); i++){
            int sum = numberOfItemsInCol(i);
            if(sum>max){
                max=sum;
                tall = i;
            }
        }
        return tall;
    }

    //Function to calculate the number of items in a column
    private int numberOfItemsInCol(int col){

        //Iterate through the column and add all the non null spots
        int sum = 0;
        for(int j = 0; j<currBoard.rows(col);j++){
            if(currBoard.getAt(col, j) != CircleColor.NONE ){
                sum+=1;
            }
        }
        //Return the sum
        return sum;
    }

    //Implementation of the safe move, and takes the tallest column as an input
    private Board.Pos safeMove(int col){

        int max = 0;
        //Best position and a copy of our board
        Board.Pos bestPos = new Board.Pos(0, 0);
        Board currStateCopy = new Board(currBoard);

        //Iterates through each row of the largest column
        for (int yy = 0; yy<currBoard.rows(col);yy++){
            int xx = col;
            //Find which click in the column will yield the largest amount of poitns
            if (currStateCopy.getAt(xx, yy) != CircleColor.NONE) {
                Board test = new Board(currStateCopy);
                currStateCopy.clickNodeHelper(xx, yy, test.getAt(xx, yy));
                int count = test.clickNode(xx, yy);
                if (count > max) {
                    max = count;
                    bestPos = new Board.Pos(xx, yy);
                }
            }
        }

        //returns the found postions
        bestPos = new Board.Pos(bestPos.xx, myGUI.boardHeight() - 1 - bestPos.yy);
        return bestPos;
    }
    //Implementation of the greedy logic provided in LazyBrain, but renamed
    private Board.Pos greedyMove() {
        // greedy choice
        int max = 0;                  // the maximum number of points for the best position found
        Board.Pos bestPos = new Board.Pos(0, 0); // the best position found
        Board currStateCopy = new Board(currBoard);

        for (int xx = 0; xx < currBoard.columns(); xx++)
            for (int yy = 0; yy < currBoard.rows(xx); yy++) {
                if (currStateCopy.getAt(xx, yy) != CircleColor.NONE) {
                    Board test = new Board(currStateCopy);

                    // mark all other nodes in the region as "clear" (but does not delete anything)
                    currStateCopy.clickNodeHelper(xx, yy, test.getAt(xx, yy));

                    // try removing the region to see what is left over
                    int count = test.clickNode(xx, yy);
                    if (count > max) {
                        // record a new best move
                        max = count;
                        bestPos = new Board.Pos(xx, yy);
                    }

                }
            }

        // convert bestPos to GUI coordinates
        bestPos = new Board.Pos(bestPos.xx, myGUI.boardHeight() - 1 - bestPos.yy);

        // return the result to the GUI
        return bestPos;
    }
}
